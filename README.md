# bridj-jest
```
This is a small app that loads vehicles and drivers from a remote endpoint via axios, adds them
to the vuex store and then displays them dynamically on a Google Maps instance.
The Google Maps key is stored in the .env file.

Tasks: 
1) Extend the drivers data with an image field (you can use an image service like unsplash.com etc. to hot-link some demo images, make sure you add a reasonable size to the url params) 
2) Update the DriverDetails component to display the driver's image from step (1)
3) Develop a form component to create a new driver, display new driver reactively in the sidebar (to get you familiar with the vuex store actions/mutations/state) use the prepared component `src/components/forms/DriverForm.vue`
4) Add function to delete a driver (click on an empty-bin icon in the top right corner of the driver's profile which then toggles a confirmation prompt which then deletes the driver from the store)


Features:
1) If a driver that is allocated to a car is clicked, a marker on the map with the position if their vehicle is displayed
2) If a vehicle is clicked, a marker of the vehicles position is displayed on the map
3) All vehicles can be shown at once on the map with a click of the button "How all vehicles on map"
4) The driver's status shows whether they are currently driving or free
5) A marker on the map can be clicked which then highlights the vehicle and the driver on the left hand panel
6) To show loading progress bar when the app is getting data from the endpoints I've added an interval of a few seconds to slow
down the calls. This can be easily deactivated and is only there for demo purposes.
7) There are two store actions to push data into the localStorage of the browser and to load localStorage items back into the vuexStore. This was to be able to cache the api data and to show immediate results (vehicles, drivers) if the app has been already loaded once. I just haven't finished this part because the API is so fast you wouldn't see any difference.

Tests:
1) Most components are covered with the JEST-based unit tests (see the tests/unit folder) 
2) Two components are also covered by JEST snapshots (see the __snapshots__ folder) which can be used to compare between different versions of the same component

Steps:

1) Install the app with `yarn install`
2) Add your Google Maps API Key to the `.env` file
3) Run the app locally with `yarn serve` which then runs the app on `http://localhost:8080` or whatever port has been allocated by the webpack dev server (you'll see that in the console)
4) To run tests "npm run test:unit
```


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run Unit Tests
```
yarn test:unit
```
