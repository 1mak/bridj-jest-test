import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    vehicles: null,
    drivers: null,
    selectedVehicle: null,
    selectedDriver: null,
    showAllVehiclesOnMap: false,
    isLoading: {
      status: false,
      type: null
    }
  },
  mutations: {
    UPDATE_FROM_LOCAL_STORAGE (state, payload) {
      state[payload.field] = payload.value
    },
    UPDATE_DRIVERS (state, payload) {
      state.drivers = payload.drivers
    },
    UPDATE_VEHICLES (state, payload) {
      state.vehicles = payload.vehicles
    },
    UPDATE_SELECTED_VEHICLE (state, payload) {
      state.selectedVehicle = payload
    },
    UPDATE_SELECTED_DRIVER (state, payload) {
      state.selectedDriver = payload
    },
    UPDATE_SHOW_ALL_VEHICLES_ON_MAP (state, payload) {
      state.showAllVehiclesOnMap = payload
    },
    UPDATE_IS_LOADING (state, payload) {
      state.isLoading = payload
    }
  },
  actions: {
    updateIsLoading({ commit }, payload) {
      commit('UPDATE_IS_LOADING', payload)
    },
    updateLocalStorage ({ state }, payload) {
      localStorage.setItem(payload, JSON.stringify(state[payload]))
    },
    localStorageToStore ({ commit }, payload) {
      if (localStorage.getItem(payload)) {
        const item = JSON.parse(localStorage.getItem(payload))
        const newPayload = { field: payload, value: item }
        commit('UPDATE_FROM_LOCAL_STORAGE', newPayload)
      }
    },
    toggleShowAllVehiclesOnMap ({ state, commit }) {
      commit('UPDATE_SHOW_ALL_VEHICLES_ON_MAP', !state.showAllVehiclesOnMap)
    },
    makeGetRequest ({ commit, dispatch }, payload) {
      // @todo: add Authorization
      dispatch('updateIsLoading', {
        status: true,
        type: payload.loadingType
      })
      setTimeout(() => {
        axios
          .get(`${payload.url}`)
          .then((response) => {
            if (response && response.data !== undefined) {
              commit(payload.commit, response.data)
              dispatch('updateIsLoading', {
                status: false,
                type: payload.loadingType
              })
              return response.data
            }
          })
          .catch((err) => {
            alert('Error in Axios Request')
            dispatch('updateIsLoading', {
              status: false,
              type: payload.loadingType
            })
            return err
          })
        }, 1500)
    },
    getDrivers ({ dispatch }) {
      const payload = {
        url: process.env.VUE_APP_API_URL_DRIVERS,
        commit: 'UPDATE_DRIVERS',
        loadingType: 'drivers'
      }
      dispatch('makeGetRequest', payload)
    },
    getVehicles ({ dispatch }) {
      const payload = {
        url: process.env.VUE_APP_API_URL_VEHICLES,
        commit: 'UPDATE_VEHICLES',
        loadingType: 'vehicles'
      }
      dispatch('makeGetRequest', payload)
    },
    selectVehicle ({ commit }, payload) {
      commit('UPDATE_SELECTED_VEHICLE', payload)
    },
    selectDriver ({ commit }, payload) {
      commit('UPDATE_SELECTED_DRIVER', payload)
    },
    selectType ({ commit }, payload) {
      commit('UPDATE_SELECTED_TYPE', payload)
    },
    selectId ({ commit }, payload) {
      commit('UPDATE_SELECTED_ID', payload)
    }
  }
})
