import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'
import App from './App.vue'
import store from './store'

// Vue.use(VueGoogleMaps, {
//   load: {
//     key: process.env.VUE_APP_GOOGLE_MAPS_API_KEY,
//     libraries: 'places'
//   },
//   installComponents: true
// })
Vue.use(VueGoogleMaps, {
  load: {
    key: process.env.VUE_APP_GOOGLE_MAPS_API_KEY
  },
  // Demonstrating how we can customize the name of the components
  installComponents: false
})

Vue.config.productionTip = false

new Vue({
  components: {
    VueGoogleMaps
  },
  store,
  render: h => h(App)
}).$mount('#app')
