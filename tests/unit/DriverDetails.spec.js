import { createLocalVue, mount, shallowMount } from '@vue/test-utils'
import DriverDetails from '@/components/drivers/DriverDetails'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('DriverDetails.vue', () => {
  let actions
  let store

  beforeEach(() => {
    actions = {
      selectVehicle: jest.fn()
    }
    store = new Vuex.Store({
      actions
    })
  })

  it('DriverDetails: Map exists.', () => {
    const wrapper = shallowMount(DriverDetails, { store,
      localVue,
      propsData: {
        driver: {
          id: 1,
          first_name: 'Ellen',
          last_name: 'Ripley'
        }
      }
    })
    expect(wrapper.find({ ref: 'driver-id' }).exists()).toBe(true)
    expect(wrapper.find({ ref: 'first-name' }).exists()).toBe(true)
    expect(wrapper.find({ ref: 'last-name' }).exists()).toBe(true)
  })

  test('DriverDetails: renders correctly', () => {
    const wrapper = mount(DriverDetails, { store,
      localVue,
      propsData: {
        driver: {
          id: 1,
          first_name: 'Ellen',
          last_name: 'Ripley'
        }
      } })
    expect(wrapper.element).toMatchSnapshot()
  })
})
