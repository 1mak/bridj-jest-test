import { createLocalVue, shallowMount } from '@vue/test-utils'
import App from '@/App'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('App.vue', () => {
  let actions
  let store

  beforeEach(() => {
    actions = {
      getVehicles: jest.fn(),
      getDrivers: jest.fn()
    }
    store = new Vuex.Store({
      actions
    })
  })

  it('App: Checks whether the sidebar component is loaded', () => {
    const wrapper = shallowMount(App, { store, localVue })
    expect(wrapper.find({ ref: 'side-bar' }).exists()).toBe(true)
  })
  
  it('App: Checks whether the google maps component is loaded', () => {
    const wrapper = shallowMount(App, { store, localVue })
    expect(wrapper.find({ ref: 'google-maps' }).exists()).toBe(true)
  })
})

