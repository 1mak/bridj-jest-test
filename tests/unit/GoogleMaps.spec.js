import { createLocalVue, mount, shallowMount } from '@vue/test-utils'
import GoogleMaps from '@/components/map/GoogleMaps'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('GoogleMaps.vue', () => {
  let actions
  let store

  beforeEach(() => {
    actions = {
      selectVehicle: jest.fn()
    }
    store = new Vuex.Store({
      actions
    })
  })

  it('GoogleMaps: Map exists.', () => {
    const wrapper = shallowMount(GoogleMaps, { store, localVue })
    expect(wrapper.find({ ref: 'google-map' }).exists()).toBe(true)
  })
  it('GoogleMaps: Marker item exists.', () => {
    const wrapper = shallowMount(GoogleMaps, { store, localVue })
    expect(wrapper.find({ ref: 'geo-marker' }).exists()).toBe(true)
  })
})
