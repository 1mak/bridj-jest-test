import { createLocalVue, mount, shallowMount } from '@vue/test-utils'
import ListDrivers from '@/components/drivers/ListDrivers'
import DriverDetails from '@/components/drivers/DriverDetails'
import drivers from '@/mockdata/drivers.js'

import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('ListDrivers.vue', () => {
  let actions
  let store

  beforeEach(() => {
    actions = {
      getDrivers: jest.fn()
    }
    store = new Vuex.Store({
      state: {
        drivers: drivers
      },
      actions
    })
  })

  it('ListDrivers: calls store action "getDrivers" when button is clicked', () => {
    const wrapper = shallowMount(ListDrivers, { store, localVue })
    wrapper.find('button').trigger('click')
    expect(actions.getDrivers).toHaveBeenCalled()
  })
  
  it('ListDrivers: load individual drivers and mounts DriversList component', () => {
    const wrapper = mount(ListDrivers, { store, localVue })
    expect(wrapper.find(DriverDetails).exists()).toBe(true)
  })
  
  test('renders correctly', () => {
    const wrapper = mount(ListDrivers, { store, localVue })
    expect(wrapper.element).toMatchSnapshot()
  })
})
