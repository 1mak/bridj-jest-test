import { shallowMount } from '@vue/test-utils'
import SideBar from '@/components/layout/SideBar.vue'

describe('SideBar.vue', () => {
  it('SideBar: Renders title when passed.', () => {
    const title = 'Choose a driver or vehicle'
    const wrapper = shallowMount(SideBar, {
      propsData: { title }
    })
    expect(wrapper.text()).toMatch(title)
  })
})
