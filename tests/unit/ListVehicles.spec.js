import { createLocalVue, shallowMount } from '@vue/test-utils'
import ListVehicles from '@/components/vehicles/ListVehicles'
import vehicles from '@/mockdata/vehicles.js'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('ListVehicles.vue', () => {
  let actions
  let store

  beforeEach(() => {
    actions = {
      getVehicles: jest.fn(),
      selectVehicle: jest.fn(),
      toggleShowAllVehiclesOnMap: jest.fn()
    }

    store = new Vuex.Store({
      state: {
        vehicles: vehicles
      },
      actions
    })
  })

  it('ListVehicles: Lists vehicles from mockdata.', () => {
    const wrapper = shallowMount(ListVehicles, { store, localVue })
    expect(wrapper.find({ ref: 'has-vehicles' }).exists()).toBe(true)
  })
  
  it('ListVehicles: Vehicle item exists.', () => {
    const wrapper = shallowMount(ListVehicles, { store, localVue })
    expect(wrapper.find({ ref: 'select-vehicle' }).exists()).toBe(true)
  })

  it('ListVehicles: calls store action "getVehicles" when button is clicked', () => {
    const wrapper = shallowMount(ListVehicles, { store, localVue })
    wrapper.find({ ref: 'load-vehicles' }).trigger('click')
    expect(actions.getVehicles).toHaveBeenCalled()
  })

  it('ListVehicles: calls store action "selectVehicle" when button is clicked', () => {
    const wrapper = shallowMount(ListVehicles, { store, localVue })
    wrapper.find({ ref: 'select-vehicle' }).trigger('click')
    expect(actions.selectVehicle).toHaveBeenCalled()
  })
  
  it('ListVehicles: calls store action "showAllVehicles" when button is clicked', () => {
    const wrapper = shallowMount(ListVehicles, { store, localVue })
    wrapper.find({ ref: 'show-all-vehicles' }).trigger('click')
    expect(actions.toggleShowAllVehiclesOnMap).toHaveBeenCalled()
  })
  
  it('ListVehicles: changes text of toggle button when clicked', () => {
    const wrapper = shallowMount(ListVehicles, { store, localVue })
    wrapper.find({ ref: 'show-all-vehicles' }).trigger('click')
    expect(wrapper.find({ ref: 'show-all-vehicles' }).text()).toMatch('Show all vehicles on map')
  })
})
